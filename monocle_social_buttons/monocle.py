appname = 'monocle_social_buttons'
context_callback =  "'monocle_social_buttons': SocialButton.objects.all()"
models = ['SocialButton']

included_app_reqs = [
    "easy_thumbnails",
]


